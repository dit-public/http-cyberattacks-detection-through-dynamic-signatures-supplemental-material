# HTTP Cyberattacks Detection through Dynamic Signatures Generation in Mutiplants IoT Factories



## Project structure
- **00_TalosRules/:** this directory contains the default (m0 type) and optimized (m2 type) Talos rules.
  - **TALOS RULES DEFAULT (m0).rules**: this file contains the the default Talos rules (m0 type).
  - **TALOS RULES OPTIMIZED (m2).rules**: this file contains the the optimized Talos rules (m2 type).
- **01_Attacks/:** this directory contains the split attacks for each of the four sites.
  - **attacks_site_1/:** attacks used for site 1.
    - **attacks_site1_1.log** 
    - **attacks_site1_2.log** 
    - **attacks_site1_3.log** 
  - **attacks_site_2/:** attacks used for site 2.
    - **attacks_site2_1.log** 
    - **attacks_site2_2.log** 
    - **attacks_site2_3.log** 
  - **attacks_site_3/:** attacks used for site 3.
    - **attacks_site3_1.log** 
    - **attacks_site3_2.log** 
    - **attacks_site3_3.log** 
  - **attacks_site_4/:** attacks used for site 4.
    - **attacks_site4_1.log** 
    - **attacks_site4_2.log** 
    - **attacks_site4_3.log** 
